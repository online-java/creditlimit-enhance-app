package com.rmsbank.cards.credit.offers.rest;

import com.rmsbank.cards.credit.offers.dto.ClePromocodeServiceResponse;
import com.rmsbank.cards.credit.offers.service.CreditLimitEnhanceServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CreditLimitEnhanceRestController.class)
@AutoConfigureMockMvc(addFilters = false)

class CreditLimitEnhanceRestControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CreditLimitEnhanceServiceImpl creditLImitEnhanceServiceI;




    @BeforeEach
    void setUp() {

    }
    @Test
    void verifyPromocode() throws Exception {

        given(this.creditLImitEnhanceServiceI.verifyPromocode("hdfc2020")).willReturn(prepareResponse());
        this.mockMvc.perform(get("/credit-limit-enhance/verify")
                .param("promocode","hdfc2020").
                accept(MediaType.APPLICATION_JSON).content("json")
                .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.promocode").value("hdfc2020"));

    }

    private ClePromocodeServiceResponse prepareResponse() {

        ClePromocodeServiceResponse clePromocodeServiceResponse=  new ClePromocodeServiceResponse();
        clePromocodeServiceResponse.setPromocode("hdfc2020");
        clePromocodeServiceResponse.setExpDate("20/9/2020");
        clePromocodeServiceResponse.setEligibleAmount(5000d);
        clePromocodeServiceResponse.setCurrentLimit(50000d);
        return clePromocodeServiceResponse;
    }

    @AfterEach
    void tearDown() {
    }


}