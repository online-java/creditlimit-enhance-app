package com.rmsbank.cards.credit.offers.intg;

import com.rmsbank.cards.credit.offers.service.CreditLimitEnhanceServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = CLETestConfig.class)
@AutoConfigureMockMvc(addFilters = false)

@TestPropertySource(locations = "classpath:application-test.properties")
class CreditLimitEnhanceRestControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;


    @Autowired
    private CreditLimitEnhanceServiceImpl creditLImitEnhanceServiceI;


    @Test
    void verifyPromocode() throws Exception {

        // given(this.creditLImitEnhanceServiceI.verifyPromocode("hdfc2020")).willReturn(prepareResponse());
        this.mockMvc.perform(get("/credit-limit-enhance/verify")
                .param("promocode", "hdfc2021").
                        accept(MediaType.APPLICATION_JSON).content("json")
                .characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.promocode").value("hdfc2021"));

    }


    @AfterEach
    void tearDown() {
    }


}