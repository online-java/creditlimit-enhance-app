package com.rmsbank.cards.credit.offers.rest;

import com.rmsbank.cards.credit.offers.dto.ClePromocodeServiceResponse;
import com.rmsbank.cards.credit.offers.exception.BusinessException;
import com.rmsbank.cards.credit.offers.exception.CleDataAccessException;
import com.rmsbank.cards.credit.offers.exception.SystemException;
import com.rmsbank.cards.credit.offers.service.CreditLImitEnhanceServiceI;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/credit-limit-enhance")
public class CreditLimitEnhanceRestController {

    private static final String healthCheckMessage = "creditlimit-enhance-service is running fine...";
    @Autowired
    private CreditLImitEnhanceServiceI creditLImitEnhanceServiceI;

    private static final Logger resourLogger = Logger.getLogger(CreditLimitEnhanceRestController.class);

  @RequestMapping(value = "/healthCheck",method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity<String> healthCheck() throws BusinessException {


     return ResponseEntity.status(200).body(healthCheckMessage);

    }
    @RequestMapping(value = "/verify",method = RequestMethod.GET,produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<ClePromocodeServiceResponse> verifyPromocode(@RequestParam("promocode") String promocode) throws BusinessException, CleDataAccessException, SystemException {

        resourLogger.debug("entered into resource layer : " + promocode);
        //valide the promocode
        // whether it is empty or its null or valid length

        ClePromocodeServiceResponse clePromocodeServiceResponse = null;
        clePromocodeServiceResponse = creditLImitEnhanceServiceI.verifyPromocode(promocode);
        resourLogger.info("response from service .." + clePromocodeServiceResponse);

        resourLogger.debug("exiting from resource layer with success response");
        return ResponseEntity.status(200).body(clePromocodeServiceResponse);


    }

    public static void main(String[] args) {
		/*CreditLimitEnhanceResource creditLimitEnhanceResource = new CreditLimitEnhanceResource();
		Response serviceResponse = creditLimitEnhanceResource.verifyPromocode("hdfc2020");
		if (serviceResponse.getEntity() instanceof CleErrorResponse) {
			CleErrorResponse cleErrorResponse = (CleErrorResponse) serviceResponse.getEntity();
			System.out.println(cleErrorResponse.getErrorCode());
			System.out.println(cleErrorResponse.getErrorMessage());
		}
		if (serviceResponse.getEntity() instanceof ClePromocodeServiceResponse) {
			ClePromocodeServiceResponse clePromocodeServiceResponse = (ClePromocodeServiceResponse) serviceResponse.getEntity();
			System.out.println(clePromocodeServiceResponse);


		}
*/
    }
}
