package com.rmsbank.cards.credit.offers.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "clepromocodeinfo")
public class ClePromocodeInfo {
    @Id

    private String promocode;

    private Double  currentLimit;

    private Double eligibleAmount;

    private String expDate;

    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public Double getCurrentLimit() {
        return currentLimit;
    }

    public void setCurrentLimit(Double currentLimit) {
        this.currentLimit = currentLimit;
    }

    public Double getEligibleAmount() {
        return eligibleAmount;
    }

    public void setEligibleAmount(Double eligibleAmount) {
        this.eligibleAmount = eligibleAmount;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }


    @Override
    public String toString() {
        return "ClePromocodeInfo{" +
                "promocode='" + promocode + '\'' +
                ", currentLimit=" + currentLimit +
                ", eligibleAmount=" + eligibleAmount +
                ", expDate='" + expDate + '\'' +
                '}';
    }
}


