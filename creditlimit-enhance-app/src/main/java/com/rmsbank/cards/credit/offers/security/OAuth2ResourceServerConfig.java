package com.rmsbank.cards.credit.offers.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

@Configuration
@EnableResourceServer
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {
    @Value( "${sso.access.token.url}" )
    private String ssoAccessTokenUrl;

    @Override
	public void configure(HttpSecurity http) throws Exception {
        http.httpBasic().disable().anonymous().and().authorizeRequests().antMatchers("/credit-limit-enhance/**").authenticated()
                .antMatchers("/public/**").permitAll();
	}
	
    @Primary
    @Bean
    public RemoteTokenServices tokenServices() {
        final RemoteTokenServices tokenService = new RemoteTokenServices();
        tokenService.setCheckTokenEndpointUrl(ssoAccessTokenUrl);
        tokenService.setClientId("myclient");
        tokenService.setClientSecret("secret");
        return tokenService;
    }
		
}
