package com.rmsbank.cards.credit.offers.exception;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CleErrorResponse {
    public CleErrorResponse(){

    }
    private String errorMessage;
    private String errorCode;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
