package com.rmsbank.cards.credit.offers.exception;/*
package com.rmsbank.cards.credit.offers.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class CleGloabalExceptionHandler implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable exception) {

        if (exception instanceof BusinessException) {

            BusinessException businessException = (BusinessException) exception;
            exception.printStackTrace();

            CleErrorResponse cleErrorResponse = prepareErrorResponse(businessException.getErrorMessage(), businessException.getErrorCode());

         //   logger.error(cleErrorResponse.getErrorMessage()+"http statuscode"+ 409);
            return Response.status(409).entity(cleErrorResponse).build();
        }
        if (exception instanceof CleDataAccessException) {
            CleDataAccessException cleDataAccessException = (CleDataAccessException) exception;
            exception.printStackTrace();
            CleErrorResponse cleErrorResponse = prepareErrorResponse(cleDataAccessException.getErrorMessage(), cleDataAccessException.getErrorCode());
            return Response.status(409).entity(cleErrorResponse).build();
            
            
        }
        if (exception instanceof SystemException) {
            SystemException systemException = (SystemException) exception;
            exception.printStackTrace();
            CleErrorResponse cleErrorResponse = prepareErrorResponse(systemException.getErrorMessage(), systemException.getErrorCode());
            return Response.status(500).entity(cleErrorResponse).build();

        }
        if (exception instanceof NullPointerException) {
            SystemException systemException = new SystemException(CleEnum.INTERNAL_SERVER_ERROR);
            exception.printStackTrace();
            CleErrorResponse cleErrorResponse = prepareErrorResponse(systemException.getErrorMessage(), systemException.getErrorCode());
            return Response.status(500).entity(cleErrorResponse).build();

        }
        else{
            exception.printStackTrace();
            SystemException systemException = new SystemException(CleEnum.INTERNAL_SERVER_ERROR);

            CleErrorResponse cleErrorResponse = prepareErrorResponse(systemException.getErrorMessage(), systemException.getErrorCode());
            return Response.status(500).entity(cleErrorResponse).build();

        }
        // to do else part
    }

    private CleErrorResponse prepareErrorResponse(String errorMessage, String errorCode) {
        CleErrorResponse cleErrorResponse = new CleErrorResponse();
        cleErrorResponse.setErrorMessage(errorMessage);
        cleErrorResponse.setErrorCode(errorCode);
        return cleErrorResponse;
    }
}
*/
