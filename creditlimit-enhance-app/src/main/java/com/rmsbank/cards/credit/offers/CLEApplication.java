package com.rmsbank.cards.credit.offers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class CLEApplication {
    public static void main(String[] args) {
        SpringApplication.run(CLEApplication.class);
    }
}