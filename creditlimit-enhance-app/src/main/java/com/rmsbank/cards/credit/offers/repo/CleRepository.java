package com.rmsbank.cards.credit.offers.repo;


import com.rmsbank.cards.credit.offers.entity.ClePromocodeInfo;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

@Profile("spring-data-jpa")
public interface CleRepository extends JpaRepository<ClePromocodeInfo, String> {

}
