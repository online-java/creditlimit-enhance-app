package com.rmsbank.cards.credit.offers.service;


import com.rmsbank.cards.credit.offers.dto.ClePromocodeServiceResponse;
import com.rmsbank.cards.credit.offers.exception.CleDataAccessException;
import com.rmsbank.cards.credit.offers.exception.SystemException;

public interface CreditLImitEnhanceServiceI {

    public ClePromocodeServiceResponse verifyPromocode(String promocde) throws CleDataAccessException, SystemException;

}
