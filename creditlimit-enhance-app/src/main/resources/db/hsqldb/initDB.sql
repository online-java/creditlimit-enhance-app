
DROP TABLE clepromocodeinfo IF EXISTS;


CREATE  TABLE clepromocodeinfo (
  promocode    VARCHAR(20) NOT NULL ,
  currentLimit    INTEGER NOT NULL ,
  eligibleAmount    INTEGER NOT NULL ,
  expDate varchar(50) ,
  PRIMARY KEY (promocode)
);
